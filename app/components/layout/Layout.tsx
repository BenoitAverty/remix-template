import RemixLogo from "./RemixLogo";

type LayoutProps = React.PropsWithChildren<{ commit?: string }>;

export default function Layout({ commit, children }: LayoutProps) {
  return (
    <div className="container mx-auto">
      <header className="text-center mt-5">
        <h1>
          <RemixLogo />
        </h1>
        <nav className="mt-1" aria-label="Main navigation">
          <ul className="flex flex-row justify-center gap-x-5">
            <li>
              <a href="https://remix.run/docs">Remix Docs</a>
            </li>
            <li>
              <a href="https://github.com/remix-run/remix">Remix on GitHub</a>
            </li>
            <li>
              <a href="https://gitlab.com/BenoitAverty/remix-template">Fork this template</a>
            </li>
          </ul>
        </nav>
      </header>
      {children}
      <footer className="text-center mt-5 border-t border-black">
        <div>
          <p>
            This template is free to use without any guarantees.
            {commit && (
              <span>
                You&apos;re viewing commit{" "}
                <span className="bg-blue-100 font-mono rounded inline px-1">
                  {commit.substring(0, 8)}
                </span>
                .
              </span>
            )}
          </p>
        </div>
      </footer>
    </div>
  );
}
