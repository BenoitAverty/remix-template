import { readFile } from "~/lib/fs.server";
import type { LoaderFunction, MetaFunction } from "remix";
import { json, useLoaderData } from "remix";

type RawHtml = string;

type IndexData = {
  readme: RawHtml;
};

export const loader: LoaderFunction = async function loader() {
  const apiBaseUrl = "https://gitlab.com/api/v4";

  let readmeContent: string;
  if (process.env.NODE_ENV === "development") {
    const readmeBuffer = await readFile("README.md");
    readmeContent = readmeBuffer.toString("utf8");
  } else {
    const readmeContentResponse = await fetch(
      apiBaseUrl + "/projects/BenoitAverty%2Fremix-template/repository/files/README.md/raw",
    );
    readmeContent = await readmeContentResponse.text();
  }

  const renderedContentResponse = await fetch(apiBaseUrl + "/markdown", {
    method: "POST",
    body: JSON.stringify({
      text: readmeContent,
    }),
    headers: {
      "Content-type": "application/json",
    },
  });

  const markdownRenderBody = await renderedContentResponse.json();
  const data: IndexData = {
    readme: markdownRenderBody.html,
  };

  return json(data);
};

export const meta: MetaFunction = () => {
  return {
    title: "Remix Template",
    description: "A remix template by BenoitAverty",
  };
};

export default function Index() {
  const data = useLoaderData<IndexData>();

  return <div dangerouslySetInnerHTML={{ __html: data.readme }} />;
}
