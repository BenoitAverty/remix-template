module.exports = {
  root: true,
  ignorePatterns: ["remix.config.js", ".eslintrc.js", "build/*", "public/build/*", "tailwind.config.js"],
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint", "prettier", "react", "react-hooks"],
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:prettier/recommended",
  ],
  rules: {
    "react/react-in-jsx-scope": "off",
    // Why the rule below : https://blog.variant.no/a-better-way-to-type-react-components-9a6460a1d4b7
    "react/function-component-definition": [2, { namedComponents: "function-declaration" }],
    "react/jsx-curly-brace-presence": [2, { children: "never", props: "never" }],
  },
  settings: {
    react: {
      version: "detect",
    },
    formComponents: [
      // Components used as alternatives to <form> for forms, eg. <Form endpoint={ url } />
      { name: "Form", formAttribute: "action" },
    ],
    linkComponents: [
      // Components used as alternatives to <a> for linking, eg. <Link to={ url } />
      { name: "Link", linkAttribute: "to" },
    ],
  },
};
