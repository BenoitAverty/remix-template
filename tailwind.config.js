module.exports = {
  // not putting "mode: jit" here because it makes the Idea plugin break. Instead,
  // the --jit option is given in the command line.
  content: ["./app/**/*.{ts,tsx}"],
  theme: {
    extend: {},
  },
  plugins: [],
}
